package main

type Queue2 struct {
	head *Node
	tail *Node
	len  int
}

func NewQueue2() *Queue2 {
	return &Queue2{len: 0}
}

func (l *Queue2) Peek() any {
	if l.head != nil {
		return l.head.value
	}
	return nil
}

func (l *Queue2) Enqueue(value any) {
	node := &Node{value: value}
	if l.head == nil {
		l.head = node
		l.tail = node
	} else {
		l.tail.next = node
		l.tail = node
	}

	l.len++
}

func (l *Queue2) Dequeue() (value any) {
	if l.head == nil {
		return nil
	}
	value = l.head.value
	l.head = l.head.next
	if l.head == nil {
		l.tail = nil
	}
	l.len--
	return
}

func (l *Queue2) Clear() {
	l.head = nil
	l.tail = nil
	l.len = 0
}

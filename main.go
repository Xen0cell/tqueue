package main

import (
	"fmt"
	"runtime"
)

func printStats(mem runtime.MemStats) {
	runtime.ReadMemStats(&mem)
	fmt.Println("mem.Alloc:", mem.Alloc)
	fmt.Println("mem.TotalAlloc:", mem.TotalAlloc)
	fmt.Println("mem.HeapAlloc:", mem.HeapAlloc)
	fmt.Println("mem.NumGC:", mem.NumGC, "\n")
}

func main() {
	var mem runtime.MemStats
	runtime.GC()

	const count = 5000000

	queue1 := NewQueue2()
	for i := 0; i < count; i++ {
		queue1.Enqueue(i)
	}
	fmt.Printf("queue1 length: %d\n", queue1.len)
	fmt.Println("stats after insert: ")
	printStats(mem)

	fmt.Println("clear queue1 and run GC")
	queue1.Clear()
	runtime.GC()
	fmt.Println("stats after clear and gc: ")
	printStats(mem)

	queue2 := NewQueue2()
	for i := 0; i < count; i++ {
		queue2.Enqueue(i)
	}
	fmt.Printf("queue2 length: %d\n", queue2.len)
	fmt.Println("stats after insert: ")
	printStats(mem)

	fmt.Println("clear queue2 and run GC")
	queue2.Clear()
	runtime.GC()
	fmt.Println("stats after clear and gc: ")
	printStats(mem)
}

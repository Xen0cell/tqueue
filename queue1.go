package main

type Queue1 struct {
	head *Node
	tail *Node
	len  int
}

func NewQueue1() *Queue1 {
	return &Queue1{len: 0}
}

func (l *Queue1) Peek() any {
	if l.head != nil {
		return l.head.value
	}
	return nil
}

func (l *Queue1) Enqueue(value any) {
	node := &Node{value: value}
	if l.head == nil {
		l.head = node
		l.tail = node
	} else {
		l.tail.next = node
		l.tail = node
	}

	l.len++
}

func (l *Queue1) Dequeue() (value any) {
	if l.head == nil {
		return nil
	}
	node := l.head
	value = node.value
	l.head = node.next
	if l.head == nil {
		l.tail = nil
	}
	node.next = nil
	l.len--
	return
}

func (l *Queue1) Clear() {
	for l.Dequeue() != nil {
	}
}
